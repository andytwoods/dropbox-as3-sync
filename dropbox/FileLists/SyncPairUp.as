package com.dropbox.FileLists
{

	public class SyncPairUp
	{
		private var dropboxFiles:Vector.<File_Loc_Ver>;
		private var localFiles:Vector.<File_Loc_Ver>;
		private var file_loc_ver_pair:File_Loc_Ver_Pair;
		private var log:Function;
		public var pairs:Object = {};

		public function SyncPairUp(dropboxFiles:Vector.<File_Loc_Ver>, localFiles:Vector.<File_Loc_Ver>,log:Function)
		{
			
			this.dropboxFiles=	dropboxFiles;
			this.localFiles	= 	localFiles;
			this.file_loc_ver_pair = new File_Loc_Ver_Pair(log);
			this.log=log;

			pairupFiles();
		}		
		
		private function pairupFiles():void
		{
			var file_loc_ver_pair:File_Loc_Ver_Pair;
			var loc_nam:String
			
			for each(var file:File_Loc_Ver in dropboxFiles){
				loc_nam=locationName(file.location,file.name);
				if(pairs.hasOwnProperty(loc_nam)){
					throw new Error("should never arise as dropbox files of the same name cannot exist in the same location");
				}
				else{
					file_loc_ver_pair = new File_Loc_Ver_Pair(log);
					file_loc_ver_pair.dropBoxFile=file;
					//trace(123,file_loc_ver_pair.dropBoxFile.name);
					pairs[loc_nam] = file_loc_ver_pair; 
				}
			}
			
			for each(   file               in localFiles){
				//trace(111,file.location, file.name);
				loc_nam=locationName(file.location,file.name);
				//found an existing pair with the correct loc_nam (dropbox file must exist there already)
				if(pairs.hasOwnProperty(loc_nam)){
					file_loc_ver_pair = pairs[loc_nam] as File_Loc_Ver_Pair;
					
					if(file_loc_ver_pair.localFile != null){
						throw new Error("should never arise as local files of the same name cannot exist in the same location");
					}
					
					else{ 	//found a pair and adding local details to it
						file_loc_ver_pair.localFile=file;
						pairs[loc_nam] = file_loc_ver_pair; 
					}
				}
						
				else{		//else must create pair.
					file_loc_ver_pair = new File_Loc_Ver_Pair(log);
					file_loc_ver_pair.localFile=file;
					pairs[loc_nam] = file_loc_ver_pair; 
				}
			}
			
		}
		
		private function locationName(location:String, name:String):String
		{
			if(location!="")return location+'/'+name;
			return name;
		}
	}
}
