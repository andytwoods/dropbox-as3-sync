package com.dropbox
{

	
	import com.dropbox.FileLists.File_Loc_Ver;
	import com.dropbox.FileLists.File_Loc_Ver_Pair;
	import com.dropbox.FileLists.Loader_Name;
	import com.dropbox.FileLists.PairHelper;
	import com.dropbox.SQLliteDB.DropboxSyncTable;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	
	import org.hamster.dropbox.DropboxClient;
	import org.hamster.dropbox.DropboxEvent;
	
	public class SyncAction extends Sprite
	{
		public function kill():void{
			if(dropAPI.hasEventListener(DropboxEvent.GET_FILE_RESULT))dropAPI.removeEventListener(DropboxEvent.GET_FILE_RESULT, 	gotFileSuccess);
			if(dropAPI.hasEventListener(DropboxEvent.GET_FILE_FAULT))dropAPI.removeEventListener(DropboxEvent.GET_FILE_FAULT, 		gotFileFail);
			if(dropAPI.hasEventListener(DropboxEvent.PUT_FILE_RESULT))dropAPI.removeEventListener(DropboxEvent.PUT_FILE_RESULT, 	uploadFileSuccess);
			if(dropAPI.hasEventListener(DropboxEvent.PUT_FILE_FAULT))dropAPI.removeEventListener(DropboxEvent.PUT_FILE_FAULT, 		uploadFileFail);
			
			this.dropAPI=	null;
			this.folder=	null;
			this.pairs=  	null;
			this.mobileDB=	null;
			
			if(localLastEdits)localLastEdits=null;
		}
		
		private var dropAPI:DropboxClient;
		private var pairs:Object;
		private var folder:String;
		private var loaders:Vector.<Loader_Name> = new Vector.<Loader_Name>;
		private var mobileDB:DropboxSyncTable;
		private var localLastEdits:Array;
		private var log:Function;
		
		public function SyncAction(dropAPI:DropboxClient, folder:String, pairs:Object, mobileDB:DropboxSyncTable, log:Function)
		{
			this.dropAPI=	dropAPI;
			this.folder=	folder;
			this.pairs=  	pairs;
			this.mobileDB=	mobileDB;
			this.log=		log;
			
			dropAPI.addEventListener(DropboxEvent.GET_FILE_RESULT, 	gotFileSuccess);
			dropAPI.addEventListener(DropboxEvent.GET_FILE_FAULT, 	gotFileFail);
			dropAPI.addEventListener(DropboxEvent.PUT_FILE_RESULT, 	uploadFileSuccess);
			dropAPI.addEventListener(DropboxEvent.PUT_FILE_FAULT, 	uploadFileFail);
			
			PairHelper.computeActions(pairs);

		}
				
		public function doActions():void
		{
			for each(var pair:File_Loc_Ver_Pair in pairs){
				
				switch(pair.action){
					case File_Loc_Ver_Pair.NOTHING: 
						if(pair.localFile && pair.localFile.revision=='' && pair.dropBoxFile){
						    mobileDB.updateRevision(pair.localFile.location, pair.localFile.name, pair.dropBoxFile.revision);
						}
						break;
	
					case File_Loc_Ver_Pair.UPDATE_DROPBOX: 
						updateDropBox(pair);
						log('update dropbox file: '+pair.localFile.location+'    '+pair.localFile.name);
						break;
					
					case File_Loc_Ver_Pair.UPDATE_LOCAL:
						updateLocal(pair);
						log('update local file: '+pair.dropBoxFile.location+'    '+pair.dropBoxFile.name);
						break;
					
					case File_Loc_Ver_Pair.DELETE_DROPBOX:
						log('delete local file: '+pair.dropBoxFile.location+'    '+pair.dropBoxFile.name);
						deleteDropBoxFile(pair.dropBoxFile);
						break;
					
					case File_Loc_Ver_Pair.DELETE_LOCAL:
						log('delete dropbox file: '+pair.localFile.location+'    '+pair.localFile.name);
						deleteLocalFile(pair.localFile.loc_name);
						mobileDB.del_LocNam(pair.localFile.loc_name);
						break;
					
					default:
						throw new Error('should be impossible');
				}
			}
		}
		
		private function deleteDropBoxFile(dropBoxFile:File_Loc_Ver):void
		{
			dropAPI.fileDelete(dropBoxFile.loc_name);
			
		}
		
		private function deleteLocalFile(loc_name:String):void
		{
			var file:File=File.applicationStorageDirectory.resolvePath(loc_name);
			if(!file.exists)throw new Error('File does not exist apparently ('+loc_name+').  Should not arise.');
			file.deleteFileAsync();
		}
	

		
		////////////////////////////////////////////////////////////////////	
		////////////////////////////////////////////////////////////////////update Local
		
		private function updateLocal(pair:File_Loc_Ver_Pair):void
		{			
			var file:File_Loc_Ver = pair.dropBoxFile;

			loaders[loaders.length]=new Loader_Name(file,dropAPI.getFile(file.loc_name)); 
		}
		
		
		protected function gotFileFail(e:DropboxEvent):void
		{
			log("failed retrieving file from dropbox file");	
		}
		
		protected function gotFileSuccess(e:DropboxEvent):void
		{
			
			var content:ByteArray = ByteArray(e.resultObject);
			
			for (var i:int; i<loaders.length; i++){
				
				if(loaders[i].loader && loaders[i].loader.data == content){
					saveLocally(content,loaders[i].file);
					loaders[i].kill();
					loaders.splice(i,1);
					break;
				}	
			}
		}
		
		private function saveLocally(content:ByteArray, loc_file:File_Loc_Ver):void
		{
			var fileType:String = "";
			var fileTypeArr:Array = loc_file.name.split(".");
			if (fileTypeArr.length>1) {
				fileType=fileTypeArr[fileTypeArr.length-1];
			}

			if(DropboxConnection.PERMITTED_FILETYPES.indexOf('*')!=-1 || 
				DropboxConnection.PERMITTED_FILETYPES.indexOf(fileType.toLowerCase())==-1) {
				log('not doing anything with this file as it is an unknown type (dont know how to save it; permitted types currently are txt, xml, jpg, png): '+ loc_file.loc_name);
			}
			
			else{
				
				var writeFile:Function;
				
				var folder:File=File.applicationStorageDirectory.resolvePath(DropboxConnection.LOCAL_DIR+'/'+loc_file.location);
				if(!folder.exists) folder.createDirectory();

				var file:File = folder.resolvePath(loc_file.name);				
				
				switch(fileType.toLowerCase()){
					case 'txt':					
					case 'xml':
						writeFile = function():void{fileStream.writeUTFBytes(String(content))};
						break;
					
					case 'jpg':
					case 'png':
						writeFile = function():void{fileStream.writeBytes(content);};
						break;
					
					case 'swf': 
					case 'mp3':
					case 'flv':
						//this is untested
						writeFile = function():void{fileStream.writeBytes(content);};
						break;
					
					default: 
						if(DropboxConnection.PERMITTED_FILETYPES.indexOf('*')==-1)throw Error('I do not know how to save files of this type: '+fileType);
						else writeFile = function():void{fileStream.writeBytes(content);};
				}
				
				////////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////////
				//encapsulating all code regarding writing file and saving to database in the below block
				
				if(writeFile){
					var fileListener:Function = function(e:Event):void{
						fileStreamListeners(false);
						fileStream.close();
						if(e.type=='ioError')	throw new Error("cannot save the file for some reason");		//on failure, get grumpy
										
						else{ //success						
							//note that for some reason file.modificationDate is 4ms off what is reported for the same file in the future if there is no pause...
							//ABOVE, turns out that fileStream.close() must be run first for accuracy.
							addToDB(file.nativePath, loc_file.location, loc_file.name, loc_file.revision,file.modificationDate); 	//on success, write to DB	
						}		
					}
						
					var fileStreamListeners:Function = function(active:Boolean):void{
						if(active){
							fileStream.addEventListener(Event.COMPLETE,fileListener);
							fileStream.addEventListener(IOErrorEvent.IO_ERROR,fileListener);
						}
						else{
							fileStream.removeEventListener(Event.COMPLETE,fileListener);
							fileStream.removeEventListener(IOErrorEvent.IO_ERROR,fileListener);	
						}
					}
					
					var fileStream:FileStream = new FileStream();
					
					fileStreamListeners(true);
					fileStream.openAsync(file, FileMode.UPDATE);
					writeFile();
				}	
				////////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////////////////////
			}	
		}	
////////////////////////////////////////////////////////////////////	
////////////////////////////////////////////////////////////////////database block
		
		private function addToDB(loc_name:String, location:String, name:String, revision:String, date:Date):void
		{		
			mobileDB.addFile(loc_name, location, name, revision,date);
		}
		
////////////////////////////////////////////////////////////////////	
////////////////////////////////////////////////////////////////////update dropbox
		
		
		private function updateDropBox(pair:File_Loc_Ver_Pair):void
		{

			var file:File_Loc_Ver = pair.localFile;
			var localFile:File=File.applicationStorageDirectory.resolvePath(pair.localFile.loc_name);
			
			//////////////////
			//////////////////anonymous function
			localFile.addEventListener(Event.COMPLETE,function(e:Event):void{
					e.target.removeEventListener(e.type,arguments.callee);
					
					var extrapFolder:String=folder;
					if(file.location!='')extrapFolder+='/'+file.location;
					
					if(!pair.dropBoxFile)	dropAPI.putFile(extrapFolder,file.name,localFile.data,'dropbox',false)	
					else 					dropAPI.putFile(extrapFolder,file.name,localFile.data,'',true);
					
					if(!localLastEdits)		localLastEdits = [];
					localLastEdits.push({folder:extrapFolder,name:file.name,localLastEdit:pair.localFile.date});
			},false,0,true);
			//////////////////anonymous function
			//////////////////
			
		if(!localFile.exists){
			throw new Error('should be impossible that file does not exist');
		}
		else localFile.load();
		}		
		
		
		
		protected function uploadFileFail(e:DropboxEvent):void
		{
			var message:String = e.resultObject;
			
			throw new Error("not all files uploaded: "+message);
		}
		
		protected function uploadFileSuccess(e:DropboxEvent):void
		{		
			var revision:String = 	e.resultObject.revision;
			
			var name:String=		e.resultObject.path;
			var location:String = 	''	//note below where the location is properly set
			name = 					name.substr(folder.length+2);
			
			if(name.indexOf('/')!=-1){
				var pos:int = name.length-name.split('').reverse().indexOf('/');
				location = name.substr(0,pos-1);
				name = name.substr(pos);
			}
			
			var d:String;
			for(var i:int=0;i<localLastEdits.length;i++){
				if(localLastEdits[i].name==name && localLastEdits[i].folder == folder){
					if(localLastEdits[i].localLastEdit){
						var date:Date=localLastEdits[i].localLastEdit;
						d=String(date)+','+String(date.milliseconds);
					}
					localLastEdits.splice(i,1);
					break;
				}
			}	
			mobileDB.updateRevisionAndDate(location,name,revision,d);
		}		
	}
}