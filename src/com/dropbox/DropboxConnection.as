﻿package com.dropbox
{
	import com.bit101.components.PushButton;
	import com.dropbox.SQLliteDB.DropboxCredentialsTable;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;

	import avmplus.getQualifiedClassName;
	
	import org.hamster.dropbox.DropboxClient;
	import org.hamster.dropbox.DropboxConfig;
	import org.hamster.dropbox.DropboxEvent;
	import org.hamster.dropbox.models.AccountInfo;

	public class DropboxConnection extends Sprite{

		private var dropAPI:DropboxClient;

		private var reqTokenKeyLabel:String = "";
		private var reqTokenSecretLabel:String = "";
		private var accTokenKeyLabel:String = "";
		private var accTokenSecretLabel:String = "";
		
		private var requestToken:String = "";
		private var theStage:Stage;
		private var webView:StageWebView;
		private var bg:Sprite;
		private var autoSync:Boolean;
		
		public var pipeLog:Function;
		public var appKey:String="add you app key";
		public var appSecret:String="add your secrey key"
		public var folderToSync:String = 'syncFolder'
			
		private var _log:String='';
		
		public static var DROPBOX_BOSS_FILETYPES:Array = ['xml','jpg','png','txt'];
		public static var LOCAL_BOSS_FILETYPES:Array = ['sav'];
		public static var PERMITTED_FILETYPES:Array = ['xml','jpg','png','txt','sav'];
		public static var LOCAL_DIR:String = 'localExpts';
		public static var dropboxDeterminesDelete:Boolean = true;
		
		//first checks to see if dropbox permissions exist on disk.
		
		public function kill():void{
			log("dropboxAS3 kill needs specfiying");

		}
		
		private function log(myLog:String):void{
			if(pipeLog)pipeLog(myLog);
			_log+='\n'+myLog;
		}
		
		public function DropboxConnection(sta:Stage,autoSync:Boolean=false):void
		{
			theStage=sta;
			this.autoSync=autoSync;
			this.pipeLog=pipeLog;
			
			for each(var fileType:String in LOCAL_BOSS_FILETYPES){
				if(DROPBOX_BOSS_FILETYPES.indexOf(fileType)!=-1) throw new Error('you have specified filetype '+fileType+' in both DROPBOX_BOSS_FILETYPES and LOCAL_BOSS_FILETYPES. This is not allowed');
			}
			
			if(PERMITTED_FILETYPES.indexOf('*')==-1){
				for each(    fileType		 in LOCAL_BOSS_FILETYPES){
					if(PERMITTED_FILETYPES.indexOf(fileType)==-1) throw new Error('you have specified filetype '+fileType+' in LOCAL_BOSS_FILETYPES but not in PERMITTED_FILETYPES.  I cannot process this filetype.');
				}
				
				for each(    fileType		 in DROPBOX_BOSS_FILETYPES){
					if(PERMITTED_FILETYPES.indexOf(fileType)==-1) throw new Error('you have specified filetype '+fileType+' in DROPBOX_BOSS_FILETYPES but not in PERMITTED_FILETYPES.  I cannot process this filetype.');
				}
			}
		}
		
		public function link():void{
			var credentialsTable:DropboxCredentialsTable= new DropboxCredentialsTable()
			var credentials:Object = credentialsTable.getCredentials();
			
			if(!credentials){
				log('need to link to dropbox app');
				getRequestToken();
			}
			else{
				log('linked to dropbox app');
				accTokenKeyLabel=credentials.key;
				accTokenSecretLabel=credentials.secret;
				initiateLinkup();
			}
		}

		
		public function initiateLinkup():void{
			var config:DropboxConfig = new DropboxConfig(appKey,appSecret);
			config.setAccessToken(accTokenKeyLabel, accTokenSecretLabel);
			dropAPI = new DropboxClient(config);
			accountInfo();
		}
		
		private function onbackground(off:Boolean=false):void{
				if(!off){
					
					bg = new Sprite;
					//obj.lineColour; 
					bg.graphics.beginFill(0x000000);
					//obj.lineThickness;
					bg.graphics.drawRect(0,0,theStage.stageWidth,theStage.stageHeight);
					bg.alpha=.8;
					theStage.addChild(bg)
				}
				else if(bg && theStage.contains(bg)){
					theStage.removeChild(bg);
					bg = null;
				}	
		}

		private function beGrantedAccess():void
		{
			
			if (StageWebView.isSupported == true)
			{
				onbackground();	
				
				webView = new StageWebView();
				
				webView.stage = theStage;
				webView.assignFocus();

				webView.viewPort = new Rectangle(theStage.stageWidth*.1,theStage.stageHeight*.1,theStage.stageWidth*.8,theStage.stageHeight*.8);
				
				var closeButton:PushButton = new PushButton;
				closeButton.label='close'
				closeButton.x=webView.viewPort.x+webView.viewPort.width-closeButton.width;
				closeButton.y=webView.viewPort.y+webView.viewPort.height+2;
				theStage.addChild(closeButton);
				closeButton.addEventListener(MouseEvent.CLICK, function(e:Event):void{
					closeButton.removeEventListener(MouseEvent.CLICK, arguments.callee);
					theStage.removeChild(closeButton);
					closeButton=null;

					webView.dispose();
					webView=null;
					onbackground(true);
					//attempt to get access token (unclear if the permissions were a success but nothing can really be done about that as webView is v limited)
					getAccessToken();
				});
				
				webView.loadURL(dropAPI.authorizationUrl);
			}
			else
			{
				log("stageWebView not supported");
			}
		}

		public function getRequestToken():void
		{	
			var config:DropboxConfig = new DropboxConfig(appKey,appSecret);
			dropAPI = new DropboxClient(config);
			dropAPI.requestToken();

			dropAPI.addEventListener(DropboxEvent.REQUEST_TOKEN_RESULT, function (evt:DropboxEvent):void
			{
				dropAPI.removeEventListener(DropboxEvent.REQUEST_TOKEN_RESULT, arguments.callee);
				var obj:Object = evt.resultObject;
				reqTokenKeyLabel = obj.key;
				reqTokenSecretLabel = obj.secret;
								
				// goto authorization web page to authorize, after that, call get access token 
				beGrantedAccess();
			});
			
			if (! dropAPI.hasEventListener(DropboxEvent.REQUEST_TOKEN_FAULT))dropAPI.addEventListener(DropboxEvent.REQUEST_TOKEN_FAULT, faultHandler);
			
		}

		private function canSync():void{
			this.dispatchEvent(new Event("canSync"));
			if(autoSync)syncFolder(folderToSync);
		}
		
		public function syncFolder(folder:String):void
		{
			var s:SyncFolder = new SyncFolder(folder, dropAPI,log);
			s.addEventListener(Event.COMPLETE,function(e:Event):void{
				//s.kill();
				//dropAPI = null;
				dispatchEvent(new Event(Event.COMPLETE));
			})
			s.start();
			
		}
		
		private function linkedUp():void{
			this.dispatchEvent(new Event("linkedUp"));
			canSync();
		}

		private function getAccessToken():void
		{
			dropAPI.accessToken();
			var handler:Function = function (evt:DropboxEvent):void
			        {
			                dropAPI.removeEventListener(DropboxEvent.ACCESS_TOKEN_RESULT, handler);
			                var obj:Object = evt.resultObject;
			                accTokenKeyLabel = obj.key
			                accTokenSecretLabel = obj.secret;
							//var saveAccessToken:saveFilesInternally= new saveFilesInternally("");
							//saveAccessToken.saveFile("dropBoxAccessDetails", accTokenKeyLabel+","+accTokenSecretLabel);
							addCredentials(accTokenKeyLabel,accTokenSecretLabel);
							log("successfully have been granted permissions by DropBox(key-"+accTokenKeyLabel+",secret:+"+accTokenSecretLabel+")");
							linkedUp();
			        };
			dropAPI.addEventListener(DropboxEvent.ACCESS_TOKEN_RESULT, handler);
			if (! dropAPI.hasEventListener(DropboxEvent.ACCESS_TOKEN_FAULT))
			{
				dropAPI.addEventListener(DropboxEvent.ACCESS_TOKEN_FAULT, faultHandler);
			}
		}
		
		private function addCredentials(key:String, secret:String):void
		{
			var credentials:DropboxCredentialsTable = new DropboxCredentialsTable();
			var success:Boolean = credentials.setCredentials(key, secret);
		}
		
		private function accountInfo():void
		{
			
			dropAPI.addEventListener(DropboxEvent.ACCOUNT_INFO_RESULT, function(evt:DropboxEvent):void
			{
				dropAPI.removeEventListener(DropboxEvent.ACCESS_TOKEN_RESULT, arguments.callee);
				var accountInfo:AccountInfo = AccountInfo(evt.resultObject);
				canSync();
			});
			
			dropAPI.addEventListener(DropboxEvent.ACCOUNT_INFO_FAULT, faultHandler);
			
			dropAPI.accountInfo();
		}


		private function faultHandler(evt:Event):void
		{
			if(webView)webView.dispose();
			log("There is an unknown issue with logging into your dropbox account. "+(evt as Object).resultObject);
		}
		
		///////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////
		//static functions		

		
		static public function dropBoxOverrides(filename:String):Boolean{	
			if(DROPBOX_BOSS_FILETYPES.indexOf('*')!=-1) return true;
			if(DROPBOX_BOSS_FILETYPES.indexOf(fileType(filename))!=-1) return true;
			return false;
		}
		
		static public function localOverrides(filename:String):Boolean{
			if(LOCAL_BOSS_FILETYPES.indexOf('*')!=-1) return true;
			if(LOCAL_BOSS_FILETYPES.indexOf(fileType(filename))!=-1) return true;
			return false;
		}
		
		static public function fileType(filename:String):String{
			var pos:int = filename.split("").reverse().indexOf(".");
			if(pos!=-1) return filename.substr(filename.length-pos);
			return ''
		}
	
	
	}

}