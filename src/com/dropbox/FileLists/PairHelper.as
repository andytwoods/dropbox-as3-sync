package com.dropbox.FileLists
{

	public class PairHelper
	{
				
		static public function computeActions(pairs:Object):void{	
			for each(var pair:File_Loc_Ver_Pair in pairs){
				pair.computeAction();
			}
		}
	}
}