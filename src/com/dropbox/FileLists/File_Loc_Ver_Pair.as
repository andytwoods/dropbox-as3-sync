package com.dropbox.FileLists
{
	import com.dropbox.DropboxConnection;

	public class File_Loc_Ver_Pair
	{
		public var dropBoxFile:File_Loc_Ver;
		public var localFile:File_Loc_Ver;
		
		public var action:String;
		
		private var log:Function;
		
		public static const UPDATE_LOCAL:String = 'updateLocal';
		public static const UPDATE_DROPBOX:String = 'updateDropbox';
		public static const DELETE_LOCAL:String = 'deleteLocal'; 
		public static const DELETE_DROPBOX:String = 'deleteDropBox'; 
		public static const NOTHING:String = 'nothing'; 
		
		private const deleteIfLocalFileType:Array = ['xml'];
		private const deleteIfLocalFileTypeKeepBackup:Array = ['xml'];
		
		public function File_Loc_Ver_Pair(log:Function){
			this.log=log;
		}		
		
		public function setLocalRevision(revision:String):void{
			localFile.revision=revision;
		}
		
		public function computeAction():void
		{			
			if	(!dropBoxFile && localFile){
				if(DropboxConnection.dropBoxOverrides(localFile.name))				action= DELETE_LOCAL;
				else if(DropboxConnection.localOverrides(localFile.name))			action= UPDATE_DROPBOX;
				else																action= NOTHING;
				
/*				if(DropboxConnection.dropboxDeterminesDelete)						action= DELETE_LOCAL;						
				else																action= UPDATE_DROPBOX;	*/
			}
			
			else if	(dropBoxFile && !localFile){
				if(DropboxConnection.dropBoxOverrides(dropBoxFile.name))			action= UPDATE_LOCAL;
				else if(DropboxConnection.localOverrides(dropBoxFile.name))			action= DELETE_DROPBOX;
				else																action= NOTHING;
		
/*				if(!DropboxConnection.dropboxDeterminesDelete)						action= DELETE_DROPBOX;
				else																action= UPDATE_LOCAL;*/
			}
				
			else if	(dropBoxFile && localFile){
				if(localFile.revision!=''){
					
					if		(int(dropBoxFile.revision) > int(localFile.revision)) 	action= UPDATE_LOCAL;	//when dropbox is more recent				
					else if	(localFile.date > localFile.lastRevisionDate) 			action= UPDATE_DROPBOX;	//when local is more recent
					else															action= NOTHING;
				}
				

				else {
					log('Issue: a file exists locally and in dropbox ('+dropBoxFile.loc_name+') but this file has not been encountered before locally. It is unclear which version (local or remote) or more recent.  No synching will be done.');
					//future programmer: I guess we could use dates to figure out which is more recent. Decided not to as this situation should
					//really never arise.  It WOULD arise if someone copy and paste the same folder to the remote and locally, but I doubt
					//this will happen.
					//Note that the local version of this file gets added to the database at the end of syncing. I wont ponder the consequences of this now
					//, as again, this situation should not arise in normal usage
					action= NOTHING;
				}

				}		
			else throw new Error("should not be possible");
		}
		
	}
}